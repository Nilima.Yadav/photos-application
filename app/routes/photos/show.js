import Route from '@ember/routing/route'
export default Route.extend({
	model(params){
        return this.store.findRecord('photo',params.id)
    },
    
    actions:{ 
        addComment(comment){
            this.get('store').createRecord('comment', {
                id: Math.floor(Date.now() / 1000),
                description: comment
            })
            this.controller.set('commentValue', null);
            this.controller.set('comments', this.get('store').peekAll('comment'));
               
        },
        deleteComment(comment) {
            let confirmation = confirm('Are you sure?');
      
            if (confirmation) {
              comment.deleteRecord();
            }
        },

        like(property) {
         
            this.controller.toggleProperty(property);
        }

    }   
});
