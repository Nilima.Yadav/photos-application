define('gallery-app/components/x-toggle', ['exports', 'ember-cli-toggle/components/x-toggle/component', 'gallery-app/config/environment'], function (exports, _component, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });


  const config = _environment.default['ember-cli-toggle'] || {};

  exports.default = _component.default.extend({
    theme: config.defaultTheme || 'default',
    defaultOffLabel: config.defaultOffLabel || 'Off::off',
    defaultOnLabel: config.defaultOnLabel || 'On::on',
    showLabels: config.defaultShowLabels || false,
    size: config.defaultSize || 'medium'
  });
});