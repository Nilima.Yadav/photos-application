define('gallery-app/adapters/typicode', ['exports', 'ember-data'], function (exports, _emberData) {
	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.default = _emberData.default.RESTAdapter.extend({
		host: 'https://jsonplaceholder.typicode.com'
	});
});