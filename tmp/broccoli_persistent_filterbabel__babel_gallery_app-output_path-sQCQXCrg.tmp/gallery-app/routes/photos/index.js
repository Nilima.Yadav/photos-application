define('gallery-app/routes/photos/index', ['exports'], function (exports) {
	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.default = Ember.Route.extend({
		model() {
			return this.store.findAll('photo');
		},

		setupController(controller, model) {
			Ember.set(controller, 'photos', model);
		},
		actions: {
			move(photo) {
				this.get('router').transitionTo('photos.show', photo.get('id'));
			}
		}
	});
});