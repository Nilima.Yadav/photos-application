define('gallery-app/serializers/photo', ['exports', 'ember-data'], function (exports, _emberData) {
	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.default = _emberData.default.RESTSerializer.extend({
		normalizeResponse(store, primaryModelClass, payload, id, requestType) {
			payload = { photos: payload };
			return this._super(store, primaryModelClass, payload, id, requestType);
		},
		normalizeSingleResponse(store, primaryModelClass, payload, id, requestType) {
			payload.photos.user = payload.photos.userId;
			return this._super(store, primaryModelClass, payload, id, requestType);
		},
		normalizeArrayResponse(store, primaryModelClass, payload, id, requestType) {
			payload.photos.forEach(photo => {
				photo.user = photo.userId;
			});
			return this._super(store, primaryModelClass, payload, id, requestType);
		}
	});
});