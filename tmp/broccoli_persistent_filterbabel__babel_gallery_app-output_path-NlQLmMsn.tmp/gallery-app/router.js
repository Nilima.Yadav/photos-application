define('gallery-app/router', ['exports', 'gallery-app/config/environment'], function (exports, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });


  const Router = Ember.Router.extend({
    location: _environment.default.locationType,
    rootURL: _environment.default.rootURL
  });

  Router.map(function () {
    this.route('photos', function () {
      this.route('show', { path: '/:photo_id/show' });
    });
  });
  exports.default = Router;
});