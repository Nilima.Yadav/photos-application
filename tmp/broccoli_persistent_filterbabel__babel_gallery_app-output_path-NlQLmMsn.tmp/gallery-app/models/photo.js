define('gallery-app/models/photo', ['exports', 'ember-data/model', 'ember-data'], function (exports, _model, _emberData) {
	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	const { attr, hasMany } = _emberData.default;

	exports.default = _model.default.extend({
		title: attr('string'),
		url: attr('string'),
		thumbnailUrl: attr('string'),
		comments: hasMany('comments')
	});
});