define('gallery-app/models/comment', ['exports', 'ember-data'], function (exports, _emberData) {
	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.default = _emberData.default.Model.extend({

		description: _emberData.default.attr('string'),
		photo: _emberData.default.belongsTo('photo')
	});
});