define('gallery-app/tests/lint/app.lint-test', [], function () {
  'use strict';

  QUnit.module('ESLint | app');

  QUnit.test('adapters/application.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'adapters/application.js should pass ESLint\n\n');
  });

  QUnit.test('adapters/comment.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'adapters/comment.js should pass ESLint\n\n');
  });

  QUnit.test('adapters/photo.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'adapters/photo.js should pass ESLint\n\n');
  });

  QUnit.test('adapters/typicode.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'adapters/typicode.js should pass ESLint\n\n');
  });

  QUnit.test('app.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'app.js should pass ESLint\n\n');
  });

  QUnit.test('components/comment-component.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/comment-component.js should pass ESLint\n\n');
  });

  QUnit.test('components/loading-component.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/loading-component.js should pass ESLint\n\n');
  });

  QUnit.test('controllers/photo.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/photo.js should pass ESLint\n\n');
  });

  QUnit.test('controllers/photo/index.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/photo/index.js should pass ESLint\n\n');
  });

  QUnit.test('controllers/photo/show.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/photo/show.js should pass ESLint\n\n');
  });

  QUnit.test('models/comment.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/comment.js should pass ESLint\n\n');
  });

  QUnit.test('models/photo.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/photo.js should pass ESLint\n\n');
  });

  QUnit.test('resolver.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'resolver.js should pass ESLint\n\n');
  });

  QUnit.test('router.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'router.js should pass ESLint\n\n');
  });

  QUnit.test('routes/photos.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/photos.js should pass ESLint\n\n');
  });

  QUnit.test('routes/photos/index.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/photos/index.js should pass ESLint\n\n');
  });

  QUnit.test('routes/photos/show.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/photos/show.js should pass ESLint\n\n');
  });

  QUnit.test('serializers/photo.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'serializers/photo.js should pass ESLint\n\n');
  });
});