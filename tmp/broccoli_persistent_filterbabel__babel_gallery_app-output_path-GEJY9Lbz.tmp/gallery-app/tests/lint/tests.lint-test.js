define('gallery-app/tests/lint/tests.lint-test', [], function () {
  'use strict';

  QUnit.module('ESLint | tests');

  QUnit.test('integration/components/comment-component-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/comment-component-test.js should pass ESLint\n\n');
  });

  QUnit.test('integration/components/loading-component-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/loading-component-test.js should pass ESLint\n\n');
  });

  QUnit.test('test-helper.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'test-helper.js should pass ESLint\n\n');
  });

  QUnit.test('unit/adapters/comment-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/adapters/comment-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/adapters/photo-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/adapters/photo-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/adapters/typicode-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/adapters/typicode-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/photo-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/photo-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/photo/index-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/photo/index-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/photo/show-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/photo/show-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/models/comment-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/models/comment-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/models/photo-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/models/photo-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/application-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/application-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/photos-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/photos-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/photos/index-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/photos/index-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/photos/show-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/photos/show-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/serializers/photo-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/serializers/photo-test.js should pass ESLint\n\n');
  });
});