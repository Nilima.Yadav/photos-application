define('gallery-app/routes/photos/show', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Route.extend({
        model(params) {
            return this.store.findRecord('photo', params.id);
        },

        actions: {
            addComment(comment) {
                this.get('store').createRecord('comment', {
                    id: Math.floor(Date.now() / 1000),
                    description: comment
                });
                this.controller.set('commentValue', null);
                this.controller.set('comments', this.get('store').peekAll('comment'));
            }
        }
    });
});