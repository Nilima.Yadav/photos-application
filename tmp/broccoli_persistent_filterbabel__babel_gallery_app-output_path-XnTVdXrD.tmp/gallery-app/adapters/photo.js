define('gallery-app/adapters/photo', ['exports', 'gallery-app/adapters/typicode'], function (exports, _typicode) {
	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.default = _typicode.default.extend({

		pathForType() {
			return 'photos';
		}
	});
});