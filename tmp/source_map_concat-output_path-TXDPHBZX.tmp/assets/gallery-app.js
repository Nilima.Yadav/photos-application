'use strict';



;define('gallery-app/adapters/application', ['exports', 'emberfire/adapters/firebase'], function (exports, _firebase) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _firebase.default.extend({});
});
;define('gallery-app/adapters/comment', ['exports', 'ember-data'], function (exports, _emberData) {
	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.default = _emberData.default.JSONAPIAdapter.extend({
		pathForType() {
			return 'comment';
		}
	});
});
;define('gallery-app/adapters/photo', ['exports', 'gallery-app/adapters/typicode'], function (exports, _typicode) {
	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.default = _typicode.default.extend({

		pathForType() {
			return 'photos';
		}
	});
});
;define('gallery-app/adapters/typicode', ['exports', 'ember-data'], function (exports, _emberData) {
	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.default = _emberData.default.RESTAdapter.extend({
		host: 'https://jsonplaceholder.typicode.com'
	});
});
;define('gallery-app/app', ['exports', 'gallery-app/resolver', 'ember-load-initializers', 'gallery-app/config/environment'], function (exports, _resolver, _emberLoadInitializers, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });


  const App = Ember.Application.extend({
    modulePrefix: _environment.default.modulePrefix,
    podModulePrefix: _environment.default.podModulePrefix,
    Resolver: _resolver.default
  });

  (0, _emberLoadInitializers.default)(App, _environment.default.modulePrefix);

  exports.default = App;
});
;define('gallery-app/components/carousel-arrow', ['exports', 'ember-carousel/components/carousel-arrow'], function (exports, _carouselArrow) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _carouselArrow.default;
    }
  });
});
;define('gallery-app/components/carousel-body', ['exports', 'ember-carousel/components/carousel-body'], function (exports, _carouselBody) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _carouselBody.default;
    }
  });
});
;define('gallery-app/components/carousel-container', ['exports', 'ember-carousel/components/carousel-container'], function (exports, _carouselContainer) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _carouselContainer.default;
    }
  });
});
;define('gallery-app/components/carousel-item', ['exports', 'ember-carousel/components/carousel-item'], function (exports, _carouselItem) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _carouselItem.default;
    }
  });
});
;define('gallery-app/components/comment-component', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Component.extend({

        didInsertElement() {}
    });
});
;define('gallery-app/components/loading-component', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Component.extend({});
});
;define('gallery-app/components/welcome-page', ['exports', 'ember-welcome-page/components/welcome-page'], function (exports, _welcomePage) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _welcomePage.default;
    }
  });
});
;define('gallery-app/components/x-toggle', ['exports', 'ember-cli-toggle/components/x-toggle/component', 'gallery-app/config/environment'], function (exports, _component, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });


  const config = _environment.default['ember-cli-toggle'] || {};

  exports.default = _component.default.extend({
    theme: config.defaultTheme || 'default',
    defaultOffLabel: config.defaultOffLabel || 'Off::off',
    defaultOnLabel: config.defaultOnLabel || 'On::on',
    showLabels: config.defaultShowLabels || false,
    size: config.defaultSize || 'medium'
  });
});
;define('gallery-app/controllers/photo', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Controller.extend({});
});
;define('gallery-app/controllers/photo/index', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Controller.extend({});
});
;define('gallery-app/controllers/photo/show', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.Controller.extend({});
});
;define('gallery-app/helpers/and', ['exports', 'ember-truth-helpers/helpers/and'], function (exports, _and) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _and.default;
    }
  });
  Object.defineProperty(exports, 'and', {
    enumerable: true,
    get: function () {
      return _and.and;
    }
  });
});
;define('gallery-app/helpers/app-version', ['exports', 'gallery-app/config/environment', 'ember-cli-app-version/utils/regexp'], function (exports, _environment, _regexp) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.appVersion = appVersion;
  function appVersion(_, hash = {}) {
    const version = _environment.default.APP.version;
    // e.g. 1.0.0-alpha.1+4jds75hf

    // Allow use of 'hideSha' and 'hideVersion' For backwards compatibility
    let versionOnly = hash.versionOnly || hash.hideSha;
    let shaOnly = hash.shaOnly || hash.hideVersion;

    let match = null;

    if (versionOnly) {
      if (hash.showExtended) {
        match = version.match(_regexp.versionExtendedRegExp); // 1.0.0-alpha.1
      }
      // Fallback to just version
      if (!match) {
        match = version.match(_regexp.versionRegExp); // 1.0.0
      }
    }

    if (shaOnly) {
      match = version.match(_regexp.shaRegExp); // 4jds75hf
    }

    return match ? match[0] : version;
  }

  exports.default = Ember.Helper.helper(appVersion);
});
;define('gallery-app/helpers/eq', ['exports', 'ember-truth-helpers/helpers/equal'], function (exports, _equal) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _equal.default;
    }
  });
  Object.defineProperty(exports, 'equal', {
    enumerable: true,
    get: function () {
      return _equal.equal;
    }
  });
});
;define('gallery-app/helpers/gt', ['exports', 'ember-truth-helpers/helpers/gt'], function (exports, _gt) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _gt.default;
    }
  });
  Object.defineProperty(exports, 'gt', {
    enumerable: true,
    get: function () {
      return _gt.gt;
    }
  });
});
;define('gallery-app/helpers/gte', ['exports', 'ember-truth-helpers/helpers/gte'], function (exports, _gte) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _gte.default;
    }
  });
  Object.defineProperty(exports, 'gte', {
    enumerable: true,
    get: function () {
      return _gte.gte;
    }
  });
});
;define('gallery-app/helpers/is-array', ['exports', 'ember-truth-helpers/helpers/is-array'], function (exports, _isArray) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _isArray.default;
    }
  });
  Object.defineProperty(exports, 'isArray', {
    enumerable: true,
    get: function () {
      return _isArray.isArray;
    }
  });
});
;define('gallery-app/helpers/is-empty', ['exports', 'ember-truth-helpers/helpers/is-empty'], function (exports, _isEmpty) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _isEmpty.default;
    }
  });
});
;define('gallery-app/helpers/is-equal', ['exports', 'ember-truth-helpers/helpers/is-equal'], function (exports, _isEqual) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _isEqual.default;
    }
  });
  Object.defineProperty(exports, 'isEqual', {
    enumerable: true,
    get: function () {
      return _isEqual.isEqual;
    }
  });
});
;define('gallery-app/helpers/lt', ['exports', 'ember-truth-helpers/helpers/lt'], function (exports, _lt) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _lt.default;
    }
  });
  Object.defineProperty(exports, 'lt', {
    enumerable: true,
    get: function () {
      return _lt.lt;
    }
  });
});
;define('gallery-app/helpers/lte', ['exports', 'ember-truth-helpers/helpers/lte'], function (exports, _lte) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _lte.default;
    }
  });
  Object.defineProperty(exports, 'lte', {
    enumerable: true,
    get: function () {
      return _lte.lte;
    }
  });
});
;define('gallery-app/helpers/not-eq', ['exports', 'ember-truth-helpers/helpers/not-equal'], function (exports, _notEqual) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _notEqual.default;
    }
  });
  Object.defineProperty(exports, 'notEq', {
    enumerable: true,
    get: function () {
      return _notEqual.notEq;
    }
  });
});
;define('gallery-app/helpers/not', ['exports', 'ember-truth-helpers/helpers/not'], function (exports, _not) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _not.default;
    }
  });
  Object.defineProperty(exports, 'not', {
    enumerable: true,
    get: function () {
      return _not.not;
    }
  });
});
;define('gallery-app/helpers/or', ['exports', 'ember-truth-helpers/helpers/or'], function (exports, _or) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _or.default;
    }
  });
  Object.defineProperty(exports, 'or', {
    enumerable: true,
    get: function () {
      return _or.or;
    }
  });
});
;define('gallery-app/helpers/pluralize', ['exports', 'ember-inflector/lib/helpers/pluralize'], function (exports, _pluralize) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _pluralize.default;
});
;define('gallery-app/helpers/singularize', ['exports', 'ember-inflector/lib/helpers/singularize'], function (exports, _singularize) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _singularize.default;
});
;define('gallery-app/helpers/xor', ['exports', 'ember-truth-helpers/helpers/xor'], function (exports, _xor) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _xor.default;
    }
  });
  Object.defineProperty(exports, 'xor', {
    enumerable: true,
    get: function () {
      return _xor.xor;
    }
  });
});
;define('gallery-app/initializers/app-version', ['exports', 'ember-cli-app-version/initializer-factory', 'gallery-app/config/environment'], function (exports, _initializerFactory, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });


  let name, version;
  if (_environment.default.APP) {
    name = _environment.default.APP.name;
    version = _environment.default.APP.version;
  }

  exports.default = {
    name: 'App Version',
    initialize: (0, _initializerFactory.default)(name, version)
  };
});
;define('gallery-app/initializers/container-debug-adapter', ['exports', 'ember-resolver/resolvers/classic/container-debug-adapter'], function (exports, _containerDebugAdapter) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    name: 'container-debug-adapter',

    initialize() {
      let app = arguments[1] || arguments[0];

      app.register('container-debug-adapter:main', _containerDebugAdapter.default);
      app.inject('container-debug-adapter:main', 'namespace', 'application:main');
    }
  };
});
;define('gallery-app/initializers/ember-data', ['exports', 'ember-data/setup-container', 'ember-data'], function (exports, _setupContainer) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    name: 'ember-data',
    initialize: _setupContainer.default
  };
});
;define('gallery-app/initializers/emberfire', ['exports', 'emberfire/initializers/emberfire'], function (exports, _emberfire) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _emberfire.default;
});
;define('gallery-app/initializers/export-application-global', ['exports', 'gallery-app/config/environment'], function (exports, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.initialize = initialize;
  function initialize() {
    var application = arguments[1] || arguments[0];
    if (_environment.default.exportApplicationGlobal !== false) {
      var theGlobal;
      if (typeof window !== 'undefined') {
        theGlobal = window;
      } else if (typeof global !== 'undefined') {
        theGlobal = global;
      } else if (typeof self !== 'undefined') {
        theGlobal = self;
      } else {
        // no reasonable global, just bail
        return;
      }

      var value = _environment.default.exportApplicationGlobal;
      var globalName;

      if (typeof value === 'string') {
        globalName = value;
      } else {
        globalName = Ember.String.classify(_environment.default.modulePrefix);
      }

      if (!theGlobal[globalName]) {
        theGlobal[globalName] = application;

        application.reopen({
          willDestroy: function () {
            this._super.apply(this, arguments);
            delete theGlobal[globalName];
          }
        });
      }
    }
  }

  exports.default = {
    name: 'export-application-global',

    initialize: initialize
  };
});
;define('gallery-app/instance-initializers/ember-data', ['exports', 'ember-data/initialize-store-service'], function (exports, _initializeStoreService) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    name: 'ember-data',
    initialize: _initializeStoreService.default
  };
});
;define('gallery-app/models/comment', ['exports', 'ember-data'], function (exports, _emberData) {
	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.default = _emberData.default.Model.extend({

		description: _emberData.default.attr('string'),
		photo: _emberData.default.belongsTo('photo')
	});
});
;define('gallery-app/models/photo', ['exports', 'ember-data/model', 'ember-data'], function (exports, _model, _emberData) {
	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	const { attr, hasMany } = _emberData.default;

	exports.default = _model.default.extend({
		title: attr('string'),
		url: attr('string'),
		thumbnailUrl: attr('string'),
		comments: hasMany('comments')
	});
});
;define('gallery-app/resolver', ['exports', 'ember-resolver'], function (exports, _emberResolver) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _emberResolver.default;
});
;define('gallery-app/router', ['exports', 'gallery-app/config/environment'], function (exports, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });


  const Router = Ember.Router.extend({
    location: _environment.default.locationType,
    rootURL: _environment.default.rootURL
  });

  Router.map(function () {
    this.route('photos', function () {
      this.route('show', { path: '/:id/show' });
    });
  });
  exports.default = Router;
});
;define('gallery-app/routes/photos', ['exports'], function (exports) {
	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.default = Ember.Route.extend({});
});
;define('gallery-app/routes/photos/index', ['exports'], function (exports) {
	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.default = Ember.Route.extend({
		model() {
			return this.store.findAll('photo');
		},

		setupController(controller, model) {
			Ember.set(controller, 'photos', model);
		},
		actions: {
			move(photo) {
				this.get('router').transitionTo('photos.show', photo.get('id'));
			}
		}
	});
});
;define('gallery-app/routes/photos/show', ['exports'], function (exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
        value: true
    });
    exports.default = Ember.Route.extend({
        model(params) {
            return this.store.findRecord('photo', params.id);
        },

        actions: {
            addComment(comment) {
                this.get('store').createRecord('comment', {
                    id: Math.floor(Date.now() / 1000),
                    description: comment
                });
                this.controller.set('commentValue', null);
                this.controller.set('comments', this.get('store').peekAll('comment'));
            }
        }
    });
});
;define('gallery-app/serializers/photo', ['exports', 'ember-data'], function (exports, _emberData) {
	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.default = _emberData.default.RESTSerializer.extend({
		normalizeResponse(store, primaryModelClass, payload, id, requestType) {
			payload = { photos: payload };
			return this._super(store, primaryModelClass, payload, id, requestType);
		},
		normalizeSingleResponse(store, primaryModelClass, payload, id, requestType) {
			payload.photos.user = payload.photos.userId;
			return this._super(store, primaryModelClass, payload, id, requestType);
		},
		normalizeArrayResponse(store, primaryModelClass, payload, id, requestType) {
			payload.photos.forEach(photo => {
				photo.user = photo.userId;
			});
			return this._super(store, primaryModelClass, payload, id, requestType);
		}
	});
});
;define('gallery-app/services/ajax', ['exports', 'ember-ajax/services/ajax'], function (exports, _ajax) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _ajax.default;
    }
  });
});
;define('gallery-app/services/firebase-app', ['exports', 'emberfire/services/firebase-app'], function (exports, _firebaseApp) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _firebaseApp.default;
});
;define('gallery-app/services/firebase', ['exports', 'emberfire/services/firebase'], function (exports, _firebase) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _firebase.default;
});
;define("gallery-app/templates/application", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "/GuadNyU", "block": "{\"symbols\":[],\"statements\":[[0,\"\\n\"],[7,\"br\"],[9],[10],[7,\"br\"],[9],[10],[7,\"br\"],[9],[10],[0,\"\\n\"],[7,\"h2\"],[9],[0,\"welcome to image gallery\"],[10],[0,\"\\n\\n\"],[1,[21,\"outlet\"],false],[0,\"\\n\"],[7,\"nav\"],[11,\"class\",\"navbar navbar-inverse navbar-fixed-top\"],[9],[0,\"\\n  \"],[7,\"div\"],[11,\"class\",\"container-fluid\"],[9],[0,\"\\n    \"],[7,\"div\"],[11,\"class\",\"navbar-header\"],[9],[0,\"\\n      \"],[7,\"a\"],[11,\"class\",\"navbar-brand\"],[11,\"href\",\"#\"],[9],[0,\"Image gallery App\"],[10],[0,\"\\n    \"],[10],[0,\"\\n    \"],[7,\"ul\"],[11,\"class\",\"nav navbar-nav\"],[9],[0,\"\\n      \"],[7,\"li\"],[11,\"class\",\"active\"],[9],[4,\"link-to\",[\"photos\"],null,{\"statements\":[[0,\"Photos\"]],\"parameters\":[]},null],[10],[0,\" \\n    \"],[10],[0,\"\\n  \"],[10],[0,\"\\n\"],[10],[0,\"\\n\"]],\"hasEval\":false}", "meta": { "moduleName": "gallery-app/templates/application.hbs" } });
});
;define("gallery-app/templates/components/comment-component", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "NA7cwXPK", "block": "{\"symbols\":[],\"statements\":[[1,[21,\"text\"],false],[0,\"\\n\"],[1,[21,\"yeild\"],false]],\"hasEval\":false}", "meta": { "moduleName": "gallery-app/templates/components/comment-component.hbs" } });
});
;define("gallery-app/templates/components/loading-component", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "DQN+czaX", "block": "{\"symbols\":[],\"statements\":[],\"hasEval\":false}", "meta": { "moduleName": "gallery-app/templates/components/loading-component.hbs" } });
});
;define("gallery-app/templates/photos", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "HGnntGvq", "block": "{\"symbols\":[],\"statements\":[[0,\"\\n\\n\"],[1,[21,\"outlet\"],false],[0,\"\\n \\n\"]],\"hasEval\":false}", "meta": { "moduleName": "gallery-app/templates/photos.hbs" } });
});
;define("gallery-app/templates/photos/index", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "Lz4MW7dg", "block": "{\"symbols\":[\"photo\",\"index\"],\"statements\":[[1,[21,\"loading-component\"],false],[0,\"\\n\"],[4,\"each\",[[23,[\"photos\"]]],null,{\"statements\":[[4,\"if\",[[27,\"lt\",[[22,2,[]],10],null]],null,{\"statements\":[[0,\"\\t\\t\\t\\t\"],[7,\"div\"],[11,\"class\",\"col-sm-2\"],[9],[0,\"\\n\\t\\t\\t\\t\\t\\t\"],[7,\"a\"],[11,\"href\",\"#\"],[11,\"data-toggle\",\"tooltip\"],[12,\"title\",[22,1,[\"title\"]]],[9],[0,\"\\n\\t\\t\\t\\t\\t\\t\\t\\t\"],[7,\"div\"],[11,\"class\",\"panel panel-default photo-item\"],[9],[0,\"\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\"],[7,\"div\"],[11,\"class\",\"panel-heading\"],[9],[0,\"\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\"],[7,\"h3\"],[11,\"class\",\"panel-title\"],[9],[1,[22,1,[\"title\"]],false],[10],[0,\"\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\"],[10],[0,\"\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\"],[7,\"div\"],[11,\"class\",\"panel-body\"],[9],[0,\"\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\"],[7,\"p\"],[9],[7,\"button\"],[9],[7,\"img\"],[12,\"src\",[22,1,[\"thumbnailUrl\"]]],[11,\"alt\",\"photo\"],[3,\"action\",[[22,0,[]],\"move\",[22,1,[]]]],[9],[10],[10],[10],[0,\"\\n\\t\\t\\t\\t\\t\\t\\t\\t\\t\\t\"],[10],[0,\"\\n\\t\\t\\t\\t\\t\\t\\t\\t\"],[10],[0,\"\\n\\t\\t\\t\\t\\t\\t\"],[10],[0,\"\\n\\t\\t\\t\\t\"],[10],[0,\"\\n\"]],\"parameters\":[]},null]],\"parameters\":[1,2]},null],[1,[21,\"outlet\"],false],[0,\"\\n\\n\\n\\n\\n\\t\\t\\t\\n\\t\\t\\n\\t\\t\\n\\t\\t\\t\\n\\t\\t\"]],\"hasEval\":false}", "meta": { "moduleName": "gallery-app/templates/photos/index.hbs" } });
});
;define("gallery-app/templates/photos/show", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "yqDrRa12", "block": "{\"symbols\":[\"comment\"],\"statements\":[[7,\"div\"],[11,\"class\",\"container\"],[9],[0,\"\\n  \"],[4,\"link-to\",[\"photos\"],null,{\"statements\":[[7,\"button\"],[11,\"class\",\"btn btn-primary btncustom\"],[11,\"id\",\"homebutton\"],[9],[0,\"Home\"],[10]],\"parameters\":[]},null],[0,\"\\n  \"],[7,\"div\"],[11,\"class\",\"row\"],[9],[0,\"\\n    \"],[7,\"h4\"],[9],[0,\"Image Title:\"],[1,[23,[\"model\",\"title\"]],false],[10],[0,\"\\n    \"],[7,\"div\"],[11,\"class\",\"col-sm-8\"],[9],[0,\"\\n      \"],[7,\"p\"],[9],[7,\"img\"],[12,\"src\",[23,[\"model\",\"url\"]]],[11,\"alt\",\"image preview\"],[11,\"id\",\"imgpreview\"],[9],[10],[10],[0,\"\\n      \"],[7,\"p\"],[11,\"class\",\"download_img\"],[9],[7,\"a\"],[11,\"href\",\"\"],[12,\"download\",[23,[\"model\",\"url\"]]],[9],[0,\"Download\"],[10],[0,\" \"],[10],[0,\"\\n    \"],[10],[0,\"\\n    \"],[7,\"div\"],[11,\"class\",\"col-sm-4\"],[9],[0,\"\\n      \"],[7,\"div\"],[11,\"class\",\"like_section\"],[9],[0,\"\\n        \"],[7,\"button\"],[11,\"class\",\"btn btn-default btn-sm \"],[11,\"id\",\"cust\"],[11,\"type\",\"button\"],[9],[0,\"\\n          \"],[7,\"span\"],[11,\"class\",\"glyphicon glyphicon-thumbs-up\"],[9],[0,\"Like \"],[10],[0,\" \\n        \"],[10],[0,\"\\n        \"],[7,\"button\"],[11,\"class\",\"btn btn-primary btncustom\"],[3,\"action\",[[22,0,[]],\"addComment\",[23,[\"commentValue\"]]]],[9],[0,\"Comment\"],[10],[0,\"  \\n      \"],[10],[0,\"\\n      \"],[7,\"section\"],[11,\"class\",\"comments\"],[9],[0,\"\\n        \"],[1,[27,\"input\",null,[[\"type\",\"placeholder\",\"value\",\"autofocus\",\"id\"],[\"text\",\"Write a Comment\",[23,[\"commentValue\"]],true,\"commentss\"]]],false],[0,\"\\n\"],[4,\"if\",[[23,[\"comments\",\"length\"]]],null,{\"statements\":[[4,\"each\",[[23,[\"comments\"]]],null,{\"statements\":[[0,\"            \"],[7,\"div\"],[11,\"class\",\"comment_section\"],[11,\"id\",\"commentdisplay\"],[9],[0,\"\\n              \"],[1,[27,\"comment-component\",null,[[\"text\"],[[22,1,[\"description\"]]]]],false],[0,\"\\n            \"],[10],[0,\"\\n\"]],\"parameters\":[1]},null]],\"parameters\":[]},{\"statements\":[[0,\"          \"],[7,\"br\"],[9],[10],[7,\"br\"],[9],[10],[0,\"\\n          \"],[7,\"p\"],[11,\"class\",\"comment-body\"],[9],[0,\" No comments yet\"],[10],[0,\"\\n\"]],\"parameters\":[]}],[0,\"      \"],[10],[0,\"    \\n    \"],[10],[0,\"  \\n  \"],[10],[0,\"\\n\"],[10],[0,\"\\n\"],[1,[21,\"outlet\"],false],[0,\"\\n\\n\\n\"]],\"hasEval\":false}", "meta": { "moduleName": "gallery-app/templates/photos/show.hbs" } });
});
;define('gallery-app/torii-providers/firebase', ['exports', 'emberfire/torii-providers/firebase'], function (exports, _firebase) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _firebase.default;
});
;

;define('gallery-app/config/environment', [], function() {
  var prefix = 'gallery-app';
try {
  var metaName = prefix + '/config/environment';
  var rawConfig = document.querySelector('meta[name="' + metaName + '"]').getAttribute('content');
  var config = JSON.parse(unescape(rawConfig));

  var exports = { 'default': config };

  Object.defineProperty(exports, '__esModule', { value: true });

  return exports;
}
catch(err) {
  throw new Error('Could not read config from meta tag with name "' + metaName + '".');
}

});

;
          if (!runningTests) {
            require("gallery-app/app")["default"].create({"name":"gallery-app","version":"0.0.0+f580a477"});
          }
        
//# sourceMappingURL=gallery-app.map
