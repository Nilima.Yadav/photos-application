'use strict';

define('gallery-app/tests/helpers/create-offline-ref', ['exports', 'firebase'], function (exports, _firebase) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = createOfflineRef;


  /**
   * Creates an offline firebase reference with optional initial data and url.
   *
   * Be sure to `stubfirebase()` and `unstubfirebase()` in your tests!
   *
   * @param  {!Object} [initialData]
   * @param  {string} [url]
   * @param  {string} [apiKey]
   * @return {!firebase.database.Reference}
   */
  function createOfflineRef(initialData, url = 'https://emberfire-tests-2c814.firebaseio.com', apiKey = 'AIzaSyC9-ndBb1WR05rRF1msVQDV6EBqB752m6o') {

    if (!_firebase.default._unStub) {
      throw new Error('Please use stubFirebase() before calling this method');
    }

    const config = {
      apiKey: apiKey,
      authDomain: 'emberfire-tests-2c814.firebaseapp.com',
      databaseURL: url,
      storageBucket: ''
    };

    let app;

    try {
      app = _firebase.default.app();
    } catch (e) {
      app = _firebase.default.initializeApp(config);
    }

    const ref = app.database().ref();

    app.database().goOffline(); // must be called after the ref is created

    if (initialData) {
      ref.set(initialData);
    }

    return ref;
  }
});
define('gallery-app/tests/helpers/destroy-firebase-apps', ['exports', 'firebase'], function (exports, _firebase) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = destroyFirebaseApps;


  const { run } = Ember;

  /**
   * Destroy all Firebase apps.
   */
  function destroyFirebaseApps() {
    const deletions = _firebase.default.apps.map(app => app.delete());
    Ember.RSVP.all(deletions).then(() => run(() => {
      // NOOP to delay run loop until the apps are destroyed
    }));
  }
});
define('gallery-app/tests/helpers/replace-app-ref', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = replaceAppRef;
  /**
   * Updates the supplied app adapter's Firebase reference.
   *
   * @param  {!Ember.Application} app
   * @param  {!firebase.database.Reference} ref
   * @param  {string} [model]  The model, if overriding a model specific adapter
   */
  function replaceAppRef(app, ref, model = 'application') {
    app.register('service:firebaseMock', ref, { instantiate: false, singleton: true });
    app.inject('adapter:firebase', 'firebase', 'service:firebaseMock');
    app.inject('adapter:' + model, 'firebase', 'service:firebaseMock');
  }
});
define('gallery-app/tests/helpers/replace-firebase-app-service', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = replaceFirebaseAppService;
  /**
   * Replaces the `firebaseApp` service with your own using injection overrides.
   *
   * This is usually not needed in test modules, where you can re-register over
   * existing names in the registry, but in acceptance tests, some registry/inject
   * magic is needed.
   *
   * @param  {!Ember.Application} app
   * @param  {!Object} newService
   */
  function replaceFirebaseAppService(app, newService) {
    app.register('service:firebaseAppMock', newService, { instantiate: false, singleton: true });
    app.inject('torii-provider:firebase', 'firebaseApp', 'service:firebaseAppMock');
    app.inject('torii-adapter:firebase', 'firebaseApp', 'service:firebaseAppMock');
  }
});
define('gallery-app/tests/helpers/stub-firebase', ['exports', 'firebase'], function (exports, _firebase) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = stubFirebase;


  /**
   * When a reference is in offline mode it will not call any callbacks
   * until it goes online and resyncs. The ref will have already
   * updated its internal cache with the changed values so we shortcut
   * the process and call the supplied callbacks immediately (asynchronously).
   */
  function stubFirebase() {
    // check for existing stubbing
    if (!_firebase.default._unStub) {
      var originalSet = _firebase.default.database.Reference.prototype.set;
      var originalUpdate = _firebase.default.database.Reference.prototype.update;
      var originalRemove = _firebase.default.database.Reference.prototype.remove;

      _firebase.default._unStub = function () {
        _firebase.default.database.Reference.prototype.set = originalSet;
        _firebase.default.database.Reference.prototype.update = originalUpdate;
        _firebase.default.database.Reference.prototype.remove = originalRemove;
      };

      _firebase.default.database.Reference.prototype.set = function (data, cb) {
        originalSet.call(this, data);
        if (typeof cb === 'function') {
          setTimeout(cb, 0);
        }
      };

      _firebase.default.database.Reference.prototype.update = function (data, cb) {
        originalUpdate.call(this, data);
        if (typeof cb === 'function') {
          setTimeout(cb, 0);
        }
      };

      _firebase.default.database.Reference.prototype.remove = function (cb) {
        originalRemove.call(this);
        if (typeof cb === 'function') {
          setTimeout(cb, 0);
        }
      };
    }
  }
});
define('gallery-app/tests/helpers/unstub-firebase', ['exports', 'firebase'], function (exports, _firebase) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = unstubFirebase;
  function unstubFirebase() {
    if (typeof _firebase.default._unStub === 'function') {
      _firebase.default._unStub();
      delete _firebase.default._unStub;
    }
  }
});
define('gallery-app/tests/integration/components/comment-component-test', ['qunit', 'ember-qunit', '@ember/test-helpers'], function (_qunit, _emberQunit, _testHelpers) {
  'use strict';

  (0, _qunit.module)('Integration | Component | comment-component', function (hooks) {
    (0, _emberQunit.setupRenderingTest)(hooks);

    (0, _qunit.test)('it renders', async function (assert) {
      // Set any properties with this.set('myProperty', 'value');
      // Handle any actions with this.set('myAction', function(val) { ... });

      await (0, _testHelpers.render)(Ember.HTMLBars.template({
        "id": "gY39LVzN",
        "block": "{\"symbols\":[],\"statements\":[[1,[21,\"comment-component\"],false]],\"hasEval\":false}",
        "meta": {}
      }));

      assert.equal(this.element.textContent.trim(), '');

      // Template block usage:
      await (0, _testHelpers.render)(Ember.HTMLBars.template({
        "id": "nwI5t21C",
        "block": "{\"symbols\":[],\"statements\":[[0,\"\\n\"],[4,\"comment-component\",null,null,{\"statements\":[[0,\"        template block text\\n\"]],\"parameters\":[]},null],[0,\"    \"]],\"hasEval\":false}",
        "meta": {}
      }));

      assert.equal(this.element.textContent.trim(), 'template block text');
    });
  });
});
define('gallery-app/tests/integration/components/loading-component-test', ['qunit', 'ember-qunit', '@ember/test-helpers'], function (_qunit, _emberQunit, _testHelpers) {
  'use strict';

  (0, _qunit.module)('Integration | Component | loading-component', function (hooks) {
    (0, _emberQunit.setupRenderingTest)(hooks);

    (0, _qunit.test)('it renders', async function (assert) {
      // Set any properties with this.set('myProperty', 'value');
      // Handle any actions with this.set('myAction', function(val) { ... });

      await (0, _testHelpers.render)(Ember.HTMLBars.template({
        "id": "YRRZjnNP",
        "block": "{\"symbols\":[],\"statements\":[[1,[21,\"loading-component\"],false]],\"hasEval\":false}",
        "meta": {}
      }));

      assert.equal(this.element.textContent.trim(), '');

      // Template block usage:
      await (0, _testHelpers.render)(Ember.HTMLBars.template({
        "id": "dvyoMo63",
        "block": "{\"symbols\":[],\"statements\":[[0,\"\\n\"],[4,\"loading-component\",null,null,{\"statements\":[[0,\"        template block text\\n\"]],\"parameters\":[]},null],[0,\"    \"]],\"hasEval\":false}",
        "meta": {}
      }));

      assert.equal(this.element.textContent.trim(), 'template block text');
    });
  });
});
define('gallery-app/tests/lint/app.lint-test', [], function () {
  'use strict';

  QUnit.module('ESLint | app');

  QUnit.test('adapters/application.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'adapters/application.js should pass ESLint\n\n');
  });

  QUnit.test('adapters/comment.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'adapters/comment.js should pass ESLint\n\n');
  });

  QUnit.test('adapters/photo.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'adapters/photo.js should pass ESLint\n\n');
  });

  QUnit.test('adapters/typicode.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'adapters/typicode.js should pass ESLint\n\n');
  });

  QUnit.test('app.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'app.js should pass ESLint\n\n');
  });

  QUnit.test('components/comment-component.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/comment-component.js should pass ESLint\n\n');
  });

  QUnit.test('components/loading-component.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'components/loading-component.js should pass ESLint\n\n');
  });

  QUnit.test('controllers/photo.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/photo.js should pass ESLint\n\n');
  });

  QUnit.test('controllers/photo/index.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/photo/index.js should pass ESLint\n\n');
  });

  QUnit.test('controllers/photo/show.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'controllers/photo/show.js should pass ESLint\n\n');
  });

  QUnit.test('models/comment.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/comment.js should pass ESLint\n\n');
  });

  QUnit.test('models/photo.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'models/photo.js should pass ESLint\n\n');
  });

  QUnit.test('resolver.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'resolver.js should pass ESLint\n\n');
  });

  QUnit.test('router.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'router.js should pass ESLint\n\n');
  });

  QUnit.test('routes/photos.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/photos.js should pass ESLint\n\n');
  });

  QUnit.test('routes/photos/index.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/photos/index.js should pass ESLint\n\n');
  });

  QUnit.test('routes/photos/show.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'routes/photos/show.js should pass ESLint\n\n');
  });

  QUnit.test('serializers/photo.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'serializers/photo.js should pass ESLint\n\n');
  });
});
define('gallery-app/tests/lint/templates.template.lint-test', [], function () {
  'use strict';

  QUnit.module('TemplateLint');

  QUnit.test('gallery-app/templates/application.hbs', function (assert) {
    assert.expect(1);
    assert.ok(true, 'gallery-app/templates/application.hbs should pass TemplateLint.\n\n');
  });

  QUnit.test('gallery-app/templates/components/comment-component.hbs', function (assert) {
    assert.expect(1);
    assert.ok(true, 'gallery-app/templates/components/comment-component.hbs should pass TemplateLint.\n\n');
  });

  QUnit.test('gallery-app/templates/components/loading-component.hbs', function (assert) {
    assert.expect(1);
    assert.ok(true, 'gallery-app/templates/components/loading-component.hbs should pass TemplateLint.\n\n');
  });

  QUnit.test('gallery-app/templates/photos.hbs', function (assert) {
    assert.expect(1);
    assert.ok(true, 'gallery-app/templates/photos.hbs should pass TemplateLint.\n\n');
  });

  QUnit.test('gallery-app/templates/photos/index.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'gallery-app/templates/photos/index.hbs should pass TemplateLint.\n\ngallery-app/templates/photos/index.hbs\n  11:56  error  Interaction added to non-interactive element  no-invalid-interactive\n  11:15  error  Do not use <button> inside an <a> element with the `href` attribute  no-nested-interactive\n');
  });

  QUnit.test('gallery-app/templates/photos/show.hbs', function (assert) {
    assert.expect(1);
    assert.ok(true, 'gallery-app/templates/photos/show.hbs should pass TemplateLint.\n\n');
  });
});
define('gallery-app/tests/lint/tests.lint-test', [], function () {
  'use strict';

  QUnit.module('ESLint | tests');

  QUnit.test('integration/components/comment-component-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/comment-component-test.js should pass ESLint\n\n');
  });

  QUnit.test('integration/components/loading-component-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'integration/components/loading-component-test.js should pass ESLint\n\n');
  });

  QUnit.test('test-helper.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'test-helper.js should pass ESLint\n\n');
  });

  QUnit.test('unit/adapters/comment-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/adapters/comment-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/adapters/photo-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/adapters/photo-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/adapters/typicode-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/adapters/typicode-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/photo-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/photo-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/photo/index-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/photo/index-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/controllers/photo/show-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/controllers/photo/show-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/models/comment-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/models/comment-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/models/photo-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/models/photo-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/application-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/application-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/photos-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/photos-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/photos/index-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/photos/index-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/routes/photos/show-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/routes/photos/show-test.js should pass ESLint\n\n');
  });

  QUnit.test('unit/serializers/photo-test.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'unit/serializers/photo-test.js should pass ESLint\n\n');
  });
});
define('gallery-app/tests/test-helper', ['gallery-app/app', 'gallery-app/config/environment', '@ember/test-helpers', 'ember-qunit'], function (_app, _environment, _testHelpers, _emberQunit) {
  'use strict';

  (0, _testHelpers.setApplication)(_app.default.create(_environment.default.APP));

  (0, _emberQunit.start)();
});
define('gallery-app/tests/unit/adapters/comment-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Adapter | comment', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let adapter = this.owner.lookup('adapter:comment');
      assert.ok(adapter);
    });
  });
});
define('gallery-app/tests/unit/adapters/photo-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Adapter | photo', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let adapter = this.owner.lookup('adapter:photo');
      assert.ok(adapter);
    });
  });
});
define('gallery-app/tests/unit/adapters/typicode-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Adapter | typicode', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let adapter = this.owner.lookup('adapter:typicode');
      assert.ok(adapter);
    });
  });
});
define('gallery-app/tests/unit/controllers/photo-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Controller | photo', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let controller = this.owner.lookup('controller:photo');
      assert.ok(controller);
    });
  });
});
define('gallery-app/tests/unit/controllers/photo/index-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Controller | photo/index', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let controller = this.owner.lookup('controller:photo/index');
      assert.ok(controller);
    });
  });
});
define('gallery-app/tests/unit/controllers/photo/show-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Controller | photo/show', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let controller = this.owner.lookup('controller:photo/show');
      assert.ok(controller);
    });
  });
});
define('gallery-app/tests/unit/models/comment-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Model | comment', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('comment', {});
      assert.ok(model);
    });
  });
});
define('gallery-app/tests/unit/models/photo-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Model | photo', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let store = this.owner.lookup('service:store');
      let model = store.createRecord('photo', {});
      assert.ok(model);
    });
  });
});
define('gallery-app/tests/unit/routes/application-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | application', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:application');
      assert.ok(route);
    });
  });
});
define('gallery-app/tests/unit/routes/photos-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | photos', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:photos');
      assert.ok(route);
    });
  });
});
define('gallery-app/tests/unit/routes/photos/index-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | photos/index', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:photos/index');
      assert.ok(route);
    });
  });
});
define('gallery-app/tests/unit/routes/photos/show-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Route | photos/show', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    (0, _qunit.test)('it exists', function (assert) {
      let route = this.owner.lookup('route:photos/show');
      assert.ok(route);
    });
  });
});
define('gallery-app/tests/unit/serializers/photo-test', ['qunit', 'ember-qunit'], function (_qunit, _emberQunit) {
  'use strict';

  (0, _qunit.module)('Unit | Serializer | photo', function (hooks) {
    (0, _emberQunit.setupTest)(hooks);

    // Replace this with your real tests.
    (0, _qunit.test)('it exists', function (assert) {
      let store = this.owner.lookup('service:store');
      let serializer = store.serializerFor('photo');

      assert.ok(serializer);
    });

    (0, _qunit.test)('it serializes records', function (assert) {
      let store = this.owner.lookup('service:store');
      let record = store.createRecord('photo', {});

      let serializedRecord = record.serialize();

      assert.ok(serializedRecord);
    });
  });
});
define('gallery-app/config/environment', [], function() {
  var prefix = 'gallery-app';
try {
  var metaName = prefix + '/config/environment';
  var rawConfig = document.querySelector('meta[name="' + metaName + '"]').getAttribute('content');
  var config = JSON.parse(unescape(rawConfig));

  var exports = { 'default': config };

  Object.defineProperty(exports, '__esModule', { value: true });

  return exports;
}
catch(err) {
  throw new Error('Could not read config from meta tag with name "' + metaName + '".');
}

});

require('gallery-app/tests/test-helper');
EmberENV.TESTS_FILE_LOADED = true;
//# sourceMappingURL=tests.map
