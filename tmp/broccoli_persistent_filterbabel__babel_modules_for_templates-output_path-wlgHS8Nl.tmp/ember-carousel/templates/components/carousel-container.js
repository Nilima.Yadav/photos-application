define("ember-carousel/templates/components/carousel-container", ["exports"], function (exports) {
  "use strict";

  exports.__esModule = true;
  exports.default = Ember.HTMLBars.template({ "id": "dGkRnxVF", "block": "{\"symbols\":[\"&default\"],\"statements\":[[14,1,[[27,\"hash\",null,[[\"item\"],[[27,\"component\",[\"carousel-item\"],[[\"register\",\"allItems\"],[[27,\"action\",[[22,0,[]],\"registerItem\"],null],[23,[\"carouselItems\"]]]]]]]],[27,\"hash\",null,[[\"previous\",\"next\"],[[27,\"action\",[[22,0,[]],\"slideLeft\"],null],[27,\"action\",[[22,0,[]],\"slideRight\"],null]]]]]],[0,\"\\n\"]],\"hasEval\":false}", "meta": { "moduleName": "ember-carousel/templates/components/carousel-container.hbs" } });
});