define('gallery-app/tests/lint/templates.template.lint-test', [], function () {
  'use strict';

  QUnit.module('TemplateLint');

  QUnit.test('gallery-app/templates/application.hbs', function (assert) {
    assert.expect(1);
    assert.ok(true, 'gallery-app/templates/application.hbs should pass TemplateLint.\n\n');
  });

  QUnit.test('gallery-app/templates/components/comment-component.hbs', function (assert) {
    assert.expect(1);
    assert.ok(true, 'gallery-app/templates/components/comment-component.hbs should pass TemplateLint.\n\n');
  });

  QUnit.test('gallery-app/templates/components/loading-component.hbs', function (assert) {
    assert.expect(1);
    assert.ok(true, 'gallery-app/templates/components/loading-component.hbs should pass TemplateLint.\n\n');
  });

  QUnit.test('gallery-app/templates/photos.hbs', function (assert) {
    assert.expect(1);
    assert.ok(true, 'gallery-app/templates/photos.hbs should pass TemplateLint.\n\n');
  });

  QUnit.test('gallery-app/templates/photos/index.hbs', function (assert) {
    assert.expect(1);
    assert.ok(false, 'gallery-app/templates/photos/index.hbs should pass TemplateLint.\n\ngallery-app/templates/photos/index.hbs\n  11:56  error  Interaction added to non-interactive element  no-invalid-interactive\n  11:15  error  Do not use <button> inside an <a> element with the `href` attribute  no-nested-interactive\n');
  });

  QUnit.test('gallery-app/templates/photos/show.hbs', function (assert) {
    assert.expect(1);
    assert.ok(true, 'gallery-app/templates/photos/show.hbs should pass TemplateLint.\n\n');
  });
});